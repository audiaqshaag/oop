class calculator {

    constructor(angka1) {
        this.angka1 = angka1;
    }

    add(angka2) {
        let res = this.angka1 + angka2;
        this.angka1 = res;
        return res;
    }

    substract(angka2) {
        let res = this.angka1 - angka2;
        this.angka1 = res;
        return res;
    }

    divide(angka2) {
        let res = this.angka1 / angka2;
        this.angka1 = res;
        return res;
    }

    multiply(angka2) {
        let res = this.angka1 * angka2;
        this.angka1 = res;
        return res;
    }

    phi() {
        return Math.PI;
    }

    cos(){
        let res=Math.cos(this.angka1);
        return res;
    }

    sin(){
        let res=Math.sin(this.angka1);
        return res;
    }

    tan(){
        let res=Math.tan(this.angka1);
        return res;
    }

    cosh(){
        let res=Math.cosh(this.angka1);
        return res;
    }

    sinh(){
        let res=Math.sinh(this.angka1);
        return res;
    }

    tanh(){
        let res=Math.tanh(this.angka1);
        return res;
    }

    exp(){
        let res=Math.exp(this.angka1);
        return res;
    }

    floor(){
        let res=Math.floor(this.angka1);
        return res;
    }

    pow(angka2){
        let res=Math.pow(this.angka1,angka2);
        return res;
    }

    sqrt(){
        let res=Math.sqrt(this.angka1);
        return res;
    }

    log(){
        let res=Math.log(this.angka1);
        return res;
    }

    log100(){
        let res=Math.log100(this.angka1);
        return res;
    }

    ceiling(){
        let res=Math.ceil(this.angka1);
        return res;
    }

}
